//READY
$(document).ready(function() {

  //SETTINGS
  var homeTitle = document.title;
  var defIn = 1350;
  var defOut = 350;
  
  //POPSTATE
  window.addEventListener('popstate', function(event){
  
        console.log('popstate fired');
        console.log(event);
        var path = event.target.location.pathname;
        var title = path;
        readyPage(title,path);

  },false); 
  
  //NON-WEBKIT
  if (!$.browser.webkit){
    
    //PAGESHOW
    window.addEventListener('pageshow', function(event){
    
          console.log('pageshow fired');
          console.log(event);
          var path = event.target.location.pathname;
          var title = path;
          readyPage(title,path);

    },false);
  }

  //MENU CLICK
  $("#Menu a").bind('click',function(event){
      
      event.preventDefault();
      var page = event.target.rel;
      var title = event.target.title;
      var url = event.target.pathname;
      
      console.log(event);
      console.log('Clicked: ' + page);
      changeState(title,url);
      
  });
  
  //LOAD PAGE
  function readyPage(title,url){
      
      //HOMEPAGE
      if(title == "/" || title == "Home"){
        title = homeTitle;
      }
      
      //SEARCH "/"
      var cutLocation = title.search("[/]");
      
      //REPLACE "/"
      if(cutLocation == 0) {
        title = title.replace("/","");
        title = title.charAt(0).toUpperCase() + title.slice(1);
      } 
      
      //SET TITLE
      document.title = title;

      //INITIALLY IF HOMEPAGE
      if(url == '/') {
        url = '/home';
      } 
      
      //WHICH PAGE IS LOADING
      //console.log("Loading page: "+ url);
      
      loadPage(url);
      
  //END READY 
  }
  
  //PUSH STATE
  function changeState(title,url){
     history.pushState(null, title, url);
     readyPage(title, url);
  //END PUSH
  }
  
  //LOAD PAGE
  function loadPage(url){
  
    //REMOVE INTRO/OUTRO
    AnimateIn = null;
    
    //ANIMATE OUT
    if(typeof AnimateOut == 'function'){
      
      $.when(AnimateOut()).done(function() {
        AnimateIn = null;
        AnimateOut = null;
        callAJAX(url);
        
      });

    }else {
      AnimateIn = null;
      $('#content').fadeOut(defOut,function(){callAJAX(url)});
    };
    
  //END LOAD
  }
  
  //CALL AJAX/////////////////////////////////////////////
  function callAJAX(someLink) {
  
    //START AJAX
    $.ajax({
      type:"GET",
      url: someLink,
      context: document.body,
      cache: false,
      dataType: "html",
      success: function(data) {
      
          
        //SHOW LOADER
        $("#ajaxLoader").css({opacity:0.8});
        
        //LOAD CONTENT	 
        $('#Shell').html('');
        $('#Shell').html(data);
      
      //END SUCCESS
      },

      complete: function(data) {			

        //ANIMATION PRESETS
        $("#content").css({opacity:0});
        
            
        //ANIMATE IN
        if(typeof AnimateIn == 'function') {
        
          $("#content").css({opacity:1});
          AnimateIn();
          
        } else {
          AnimateOut = null;
          $('#content').animate({'opacity': 1}, defIn);	
        
        };
            
          
          //HIDE PAGE LOADER
          $("#ajaxLoader").fadeTo(250, 0);

      //END COMPLETE
      }

    //END AJAX
    });

  //END CALL
  }
  
//END DOC
});

//AUTHORED BY JAFAR AMJAD MARCH 1st, 2012